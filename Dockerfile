FROM python:3.7 AS builder
COPY requirements.txt .
RUN pip install --user -r requirements.txt
FROM python:3.7-slim
EXPOSE 5000
WORKDIR /app
COPY --from=builder /root/.local /root/.local
COPY ./app .
ENV PATH=/root/.local/bin:$PATH
WORKDIR /
ENV PYTHONPATH "${PYTHONPATH}:."
CMD [ "python", "app/main.py" ]
